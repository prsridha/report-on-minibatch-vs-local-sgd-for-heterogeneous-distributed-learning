import os
from datetime import datetime
import argparse
from torch.multiprocessing import Process
import torchvision
import torchvision.transforms as transforms
import torch
import torch.nn as nn
import torch.distributed as dist

class ConvNet(nn.Module):
    def __init__(self, num_classes=10):
        super(ConvNet, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv2d(1, 16, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.layer2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.fc = nn.Linear(7*7*32, num_classes)

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        out = out.reshape(out.size(0), -1)
        out = self.fc(out)
        return out

def average_gradients(model):
    """ Gradient averaging. """
    size = float(dist.get_world_size())
    for param in model.parameters():
        dist.all_reduce(param.grad.data, op=dist.ReduceOp.SUM)
        param.grad.data /= size

def train(rank, args):
    # rank = dist.get_rank()
    print("Rank - ", rank)

    torch.manual_seed(0)
    model = ConvNet()
    torch.cuda.set_device("cuda:0" if torch.cuda.is_available() else "cpu")
    model.cuda(0)
    batch_size = args.batch_size
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), lr=1e-4)

    # Data loading code
    train_dataset = torchvision.datasets.MNIST(root='./data',
                                               train=True,
                                               transform=transforms.ToTensor(),
                                               download=True)
    train_sampler = torch.utils.data.distributed.DistributedSampler(
    	train_dataset,
    	num_replicas=args.world_size,
    	rank=rank
    )
    train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                               batch_size=batch_size,
                                               shuffle=False,
                                               num_workers=0,
                                               pin_memory=True,
                                               sampler=train_sampler)

    start = datetime.now()
    total_step = len(train_loader)
    for epoch in range(args.epochs):
        for i, (images, labels) in enumerate(train_loader):
            images = images.cuda(non_blocking=True)
            labels = labels.cuda(non_blocking=True)

            # Forward pass
            outputs = model(images)
            loss = criterion(outputs, labels)

            # Backward and optimize
            optimizer.zero_grad()
            loss.backward()

            average_gradients(model)

            optimizer.step()
            if (i + 1) % 100 == 0 and rank == 0:
                print('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}'.format(
                    epoch + 1,
                    args.epochs,
                    i + 1,
                    total_step,
                    loss.item())
                )
    if rank == 0:
        print("Training complete in: " + str(datetime.now() - start))


def init_processes(rank, args):
    """ Initialize the distributed environment. """
    os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_PORT'] = '29500'
    dist.init_process_group(args.backend, rank=rank,
                            world_size=args.world_size)
    train(rank, args)


def download_data():
    train_dataset = torchvision.datasets.MNIST(root='./data',
                                               train=True,
                                               transform=transforms.ToTensor(),
                                               download=True)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--cpus', default=2, type=int,
                        help='number of cpus/processes')
    parser.add_argument('-e', '--epochs', default=2, type=int,
                        metavar='N', help='number of total epochs to run')
    parser.add_argument('-b', '--batch_size', default=64,
                        type=int, help='batch size')
    parser.add_argument('-d', '--download', default=False,
                        type=bool, help='backend type')
    parser.add_argument('--backend', default='gloo',
                        type=str, help='backend type')
    args = parser.parse_args()

    args.world_size = args.cpus

    if (args.download):
        download_data()

    processes = []
    for rank in range(args.world_size):
        p = Process(target=init_processes, args=(rank, args))
        p.start()
        processes.append(p)

    for p in processes:
        p.join()

if __name__ == '__main__':
    main()
