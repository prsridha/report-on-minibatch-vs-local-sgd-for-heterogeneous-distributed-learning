from __future__ import print_function
import os
from torch.cuda import device_count
import torch.distributed as dist
from torch.autograd import Variable
from torch.multiprocessing import Process, set_start_method
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.optim.lr_scheduler import StepLR

class LogisticRegression(torch.nn.Module):
    def __init__(self, input_dim=784, output_dim=10):
        super(LogisticRegression, self).__init__()
        self.linear = torch.nn.Linear(input_dim, output_dim)

    def forward(self, x):
        outputs = torch.sigmoid(self.linear(x))
        return outputs


def average_gradients(model):
    """ Gradient averaging. """
    size = float(dist.get_world_size())
    for param in model.parameters():
        dist.all_reduce(param.grad.data, op=dist.ReduceOp.SUM)
        param.grad.data /= size

def train_logist(args, model, train_loader, optimizer, epoch, rank, device):
    model.train()
    for batch_idx, (images, labels) in enumerate(train_loader):
        images = Variable(images.view(-1, 28 * 28))
        labels = Variable(labels)

        images, labels = images.to(device), labels.to(device)
        optimizer.zero_grad()
        outputs = model(images)
        criterion = torch.nn.CrossEntropyLoss()
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        if (batch_idx % args.log_interval == 0) and rank == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx *
                len(images), len(train_loader.dataset)/dist.get_world_size(),
                100. * batch_idx / len(train_loader), loss.item()))
            if args.dry_run:
                break
    average_gradients(model)

def test_logist(model, device, test_loader):
    model.eval()
    total = 0
    correct = 0
    with torch.no_grad():
        for images, labels in test_loader:
            images = Variable(images.view(-1, 28*28))
            images, labels = images.to(device), labels.to(device)

            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum()
        accuracy = 100 * correct/total
        print("Accuracy - ", accuracy)


def init_processes(train_dataset, device, rank, args, home):
    """ Initialize the distributed environment. """
    os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_PORT'] = '29500'
    dist.init_process_group(args.backend, rank=rank,
                            world_size=args.world_size)

    torch.manual_seed(args.seed)

    model = LogisticRegression().to(device)

    optimizer = optim.SGD(model.parameters(), lr=args.lr)

    train_sampler = torch.utils.data.distributed.DistributedSampler(
    	train_dataset,
    	num_replicas=args.world_size,
    	rank=rank,
        shuffle=True
    )
    train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                               batch_size=args.batch_size,
                                               shuffle=False,
                                               num_workers=0,
                                               pin_memory=True,
                                               sampler=train_sampler)

    for epoch in range(1, args.epochs + 1):
        train_logist(args, model, train_loader, optimizer, epoch, rank, device)
        
    if args.save_model and rank == 0:
        torch.save(model.state_dict(), home + "/mnist_cnn.pt")


def main(home):
    set_start_method('spawn')
    # Training settings
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
    parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=14, metavar='N',
                        help='number of epochs to train (default: 14)')
    parser.add_argument('--lr', type=float, default=1.0, metavar='LR',
                        help='learning rate (default: 1.0)')
    parser.add_argument('--gamma', type=float, default=0.7, metavar='M',
                        help='Learning rate step gamma (default: 0.7)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
    parser.add_argument('--dry-run', action='store_true', default=False,
                        help='quickly check a single pass')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
                        help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before logging training status')
    parser.add_argument('--save-model', action='store_true', default=False,
                        help='For Saving the current Model')
    parser.add_argument('-c', '--cpus', default=2, type=int,
                        help='number of cpus/processes')
    parser.add_argument('--backend', default='gloo',
                        type=str, help='backend type')
    parser.add_argument('--test-only', action='store_true', default=False,
                        help="to only test an existing model")
    args = parser.parse_args()
    args.world_size = args.cpus

    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.1307,), (0.3081,))
    ])
    train_dataset = datasets.MNIST('/data', train=True, download=True,
                                   transform=transform)
    test_dataset = datasets.MNIST('/data', train=False,
                                  transform=transform)

    use_cuda = not args.no_cuda and torch.cuda.is_available()
    if use_cuda:
        device = "cuda:0"
        torch.cuda.set_device(device)
    else:
        device = torch.device("cpu")

    if not args.test_only:
        processes = []
        for rank in range(args.world_size):
            p = Process(target=init_processes, args=(
                train_dataset, device, rank, args, home))
            p.start()
            processes.append(p)

        for p in processes:
            p.join()
    else:
        model = LogisticRegression().to(device)
        model.load_state_dict(torch.load(home + "/mnist_cnn.pt"))
        test_kwargs = {'batch_size': args.test_batch_size}
        if use_cuda:
            cuda_kwargs = {'num_workers': 1,
                           'pin_memory': True,
                           'shuffle': True}
            test_kwargs.update(cuda_kwargs)
        test_loader = torch.utils.data.DataLoader(test_dataset, **test_kwargs)
        test_logist(model, device, test_loader)


if __name__ == '__main__':
    home = ""
    main(home)
